/*
* Program Name : Assignment02 AuthenticProtocol.cpp
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : Implement the Authentic Protocol
* Date         : Nov. 11, 2016
* Coder        : Jaehuhn Park
*/

#include "AuthenticProtocol.h"

cSendAuthMsg::cSendAuthMsg()
{
	this->mPacket_length = 0;
	this->mType = UNKNOWN_REQUEST;
	this->passwordLength= 0;
	this->password = "";
	this->dateLength= 0;
	this->dateString = "";
	this->pBuffer = 0;
}

void cSendAuthMsg::writeToBuffer()
{
	this->pBuffer->clear();
	this->pBuffer->writeInt32BE(this->mPacket_length);
	this->pBuffer->writeInt32BE(this->mType);
	this->pBuffer->writeInt32BE(this->emailLength);
	this->pBuffer->writeStringBE(this->email);
	this->pBuffer->writeInt32BE(this->passwordLength);
	this->pBuffer->writeStringBE(this->password);
	this->pBuffer->writeInt32BE(this->dateLength);
	this->pBuffer->writeStringBE(this->dateString);
}  


void cSendAuthMsg::update()
{
	this->emailLength = this->email.length();
	this->passwordLength = this->password.length();
	this->dateLength = this->dateString.length();
	this->mPacket_length = 4 + 4 + 4 + 4 + 4 + this->emailLength + this->passwordLength + this->dateLength;
}  //The integer lenghts of packet + type + email + password + date



void cRecvAuthMsg::readFromBuffer()
{
	this->mPacket_length = this->pBuffer->readInt32BE();
	this->mType = (eAuthMsgType)this->pBuffer->readInt32BE();
	this->emailLength = this->pBuffer->readInt32BE();
	this->email = this->pBuffer->readStringBE(this->emailLength);
	this->passwordLength = this->pBuffer->readInt32BE();
	this->password = this->pBuffer->readStringBE(this->passwordLength);
	this->dateLength = this->pBuffer->readInt32BE();
	this->dateString = this->pBuffer->readStringBE(this->dateLength);
	this->pBuffer->clear();
}

