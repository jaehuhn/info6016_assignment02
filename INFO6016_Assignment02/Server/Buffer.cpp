/*
* Program Name : Assignment01 Buffer.cpp for Server
* Purpose      : Assignment #1 - INFO-6016 Network Programming
* Description  : Buffer Implementation
* Date         : Oct. 21, 2016
* Coder        : Jaehuhn Park
*/
#include "Buffer.h"
#include <stdio.h>

//Constructor - Initialize with size N
Buffer::Buffer(std::size_t size)
{
	_buffer.resize(size);
	readIndex = 0;
	writeIndex = 0;
}

//Desctructor
Buffer::~Buffer()
{
}

/********************** Serialize, Deserialize int *******************************/
//To write 32bit Integer for Big Endian byte order
void Buffer::writeInt32BE(std::size_t index, int value)
{
	if (index + 1 > _buffer.size())
		this->growBuffer(4);

	_buffer[index + 3] = value;
	_buffer[index + 2] = value >> 8;
	_buffer[index + 1] = value >> 16;
	_buffer[index] = value >> 24;
}
void Buffer::writeInt32BE(int value)
{
	writeInt32BE(writeIndex, value);
	writeIndex += 4; //increment by 4 because the vector element size is of 8bit unsigned character type
}

//To read 32bit Integer for Big Endian byte order
int Buffer::readInt32BE(std::size_t index)
{
	int value = _buffer[index + 3];
	value |= _buffer[index + 2] << 8;
	value |= _buffer[index + 1] << 16;
	value |= _buffer[index] << 24;
	return value;
}
int Buffer::readInt32BE()
{
	int value = readInt32BE(readIndex);
	readIndex += 4;
	return value;
}

/********************** Serialize, Deserialize short *******************************/
//To write 16bit short integer for Big Endian byte order
void Buffer::writeShort16BE(std::size_t index, short int value)
{
	if (index + 1 > _buffer.size())
		this->growBuffer(2);
	_buffer[index + 1] = value;
	_buffer[index] = value >> 8;
}
void Buffer::writeShort16BE(short int value)
{
	writeShort16BE(writeIndex, value);
	writeIndex += 2;
}
////To read 16bit short integer for Big Endian byte order
short int Buffer::readShort16BE(std::size_t index)
{
	short int value = _buffer[index + 1];
	value |= _buffer[index] << 8;
	return value;
}
short int Buffer::readShort16BE()
{
	short int value = readShort16BE(readIndex);
	readIndex += 2;
	return value;
}

/********************** Serialize, Deserialize string **************************/
//Write a string to the buffer
void Buffer::writeStringBE(std::size_t index, std::string value)
{
	if (index + value.length() > _buffer.size()) 
		this->growBuffer(value.length());
	for (size_t i = 0; i < value.size(); i++)
	{
		_buffer[index + i] = value.at(i);
	}
}
void Buffer::writeStringBE(std::string value)
{
	writeStringBE(writeIndex, value);
	writeIndex += value.size();
}

//Read string from the buffer
std::string Buffer::readStringBE(std::size_t stringLength)
{
	std::string  value = readStringBE(readIndex, stringLength);
	readIndex += stringLength;
	return value;
}
std::string Buffer::readStringBE(std::size_t index, std::size_t stringLength)
{
	std::string value = "";
	for (size_t i = 0; i < stringLength; i++)
	{
		char temp = _buffer[index + i];
		value.append(1, temp);
	}
	return value;
}



//Get the read index
std::size_t Buffer::getReadIndex()
{
	return readIndex;
}

//Get the write index
std::size_t Buffer::getWriteIndex()
{
	return writeIndex;
}


//Set the read index
void Buffer::setReadIndex(std::size_t index)
{
	readIndex = index;
}

//Set the write index
void Buffer::setWriteIndex(std::size_t index)
{
	writeIndex = index;
}

//Reset read and write index and clear buffer
void Buffer::clear()
{
	writeIndex = 0;
	readIndex = 0;
	_buffer.clear();
}

//Append to the vectors buffer
void Buffer::growBuffer(size_t growAmount)
{
	_buffer.resize(_buffer.size() + growAmount);
}

//Load this buffer from the static buffer used in recieve function
void Buffer::loadBuffer(char recvbuf[512], size_t size)
{
	//Grow buffer if new value will pass current size
	if (size + writeIndex > _buffer.size())
		this->growBuffer(size);

	//Add each byte from the static buffer to class memeber buffer
	for (size_t i = 0; i < size; ++i)
	{
		_buffer.at(writeIndex) = recvbuf[i];
		writeIndex++;
	}
}

//Translate buffer to const char* for send function
const char* Buffer::toCharArray()
{
	//Reset the place holder
	this->sendingString = "";

	int currentBufferLength = writeIndex - readIndex;

	//Add each byte in the buffer to a string for easy conversion to char*
	for (size_t i = 0; i < currentBufferLength; i++)
	{
		char temp = _buffer[readIndex + i];
		this->sendingString.append(1, temp);
	}

	//return a char* for the string
	return this->sendingString.c_str();
}

int Buffer::getBufferSize()
{
	return (int)this->_buffer.size();
}

//Print the content of the buffer in hex. (Debug tool)
void Buffer::printInHex()
{
	//loop over each byte and prsize_t it's hex value
	for (std::vector<uint8_t>::iterator it = _buffer.begin(); it != _buffer.end(); ++it)
	{
		printf("%02x ", *it);
	}
	printf("\n");
}