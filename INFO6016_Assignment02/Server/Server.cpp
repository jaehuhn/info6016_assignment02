/*
* Program Name : Assignment02 Server.cpp
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : using Google Protocol Buffer 
* Date         : Nov. 11, 2016
* Coder        : Jaehuhn Park
*/

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "Buffer.h"
#include "ProtocolElements.h" // for protocol between clients and this server
#include "Authentication.pb.h" //to use Google Buffer Protocol
#include "AuthenticProtocol.h" // for protocol between this server and the Authentication Server


// for windows socket link (link with Ws2_32.lib)
#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"
#define DEFAULT_BUFLEN 512

int main() 
{

	//instantiate a buffer object with size of 0
	Buffer buffer(0);
	Buffer authBuffer(0);
	bool bAuthenticProcess;

	std::vector<cUserInfo*> vUserInfo; 
	int userCount = 0; //will be the index of the vector vUserInfo


	//Create a Windows Socket Application Data Object
	WSADATA wsaData; 
	//to give Winsock version number and store the DLL information into wsaData
	int resultInt = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}
	struct addrinfo *result = NULL;
	struct addrinfo hints;

#pragma region Connection To Authentication Server

	/******* process the communtication with Authentication server *************/
	//now this server acts as a client
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	resultInt = getaddrinfo("127.0.0.1", "27016", &hints, &result);
	if (resultInt != 0) {
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup();
		return 1;
	}
	SOCKET ConnectSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Socket failed with error: " << WSAGetLastError() << std::endl;
		WSACleanup();
	}
	resultInt = connect(ConnectSocket, result->ai_addr, (int)result->ai_addrlen);
	if (resultInt == SOCKET_ERROR)
	{
		ConnectSocket = INVALID_SOCKET;
	}
	else std::cout << "Connected to the Authenication Server" << std::endl;

	freeaddrinfo(result);
	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to the Authenication Server" << std::endl;
		WSACleanup();
	}

#pragma endregion

#pragma region Prepare for clients connections

	ZeroMemory(&hints, sizeof(hints)); //hints is filled with zeros now

	hints.ai_family = AF_INET; //IPv4 Internet protocols  
	hints.ai_socktype = SOCK_STREAM; //Socket type is TCP
	hints.ai_protocol = IPPROTO_TCP; //Protocol is TCP
	hints.ai_flags = AI_PASSIVE; //for wildcard IP address

	resultInt = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result); //result will contain response information about the host
	if (resultInt != 0)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup(); //to clean up the WinSock instance
		return 1;
	}
#pragma endregion

#pragma region Listening_socket_Create_&_Bind
	SOCKET listeningSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		WSACleanup(); 
		return 1;
	}
	u_long iMode = 1;
	//To enable non-blocking mode, pass FIONBIO and non zero value of iMode
	ioctlsocket(listeningSocket, FIONBIO, &iMode);

	//Bind the socket
	resultInt = bind(listeningSocket, result->ai_addr, (int)result->ai_addrlen);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket binding failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); 
		return 1;
	}
	freeaddrinfo(result); 
#pragma endregion

#pragma region Listening
	if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Socket listening failed" << std::endl;
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}
	//now waiting for the call
	std::cout << "Listening on Socket...." << std::endl<< std::endl;

	/*To handle concurrency by Non-blocking using Select()  */
	fd_set master;    //Master list
	fd_set read_fds;  //will copy Master for select() 

	//Clear the master and the temporary sets
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	//Add the listening Socket to the master set
	FD_SET(listeningSocket, &master);

	int new_fd;        //Newly accepted socket descriptor
	int fdmax = listeningSocket; //Maximum file descriptor number 

	//Set timeout waitng  duration
	struct timeval timeValue;
	timeValue.tv_sec = 0;
	timeValue.tv_usec = 500 * 1000; // 500 ms
#pragma endregion

	cUserInfo* newUser = 0;

#pragma region MAIN_LOOP
	while (true) 
	{		
		read_fds = master; // copy the master to the temp fd_set
		//select() checks to see if any socket has activity
		if (select(fdmax + 1, &read_fds, NULL, NULL, &timeValue) == -1)
		{
			std::cout << "Select Error" << std::endl;
			exit(4);
		}

		//Loop through existing connections looking for data to read
		for (int i_fd = 0; i_fd <= fdmax; i_fd++)
		{
			if (!FD_ISSET(i_fd, &read_fds)) continue;

			if (i_fd == listeningSocket)
			{
				sockaddr_in client_addr;
				socklen_t addrlen = sizeof sockaddr_storage;

				new_fd = accept(listeningSocket, (struct sockaddr *)&client_addr, &addrlen);
				std::cout << "Connected to: " << client_addr.sin_addr.s_addr << std::endl;
				if (new_fd == -1)
				{
					std::cout << "Accept Error" << std::endl;
					continue;
				}
				FD_SET(new_fd, &master); // add to master set				
				if (new_fd > fdmax)	fdmax = new_fd; //Keep track of max fd

				std::cout << "New connection on socket : " << new_fd << std::endl;

				//Instantiation of a new cUserInfo object in the heap
				newUser = new cUserInfo();
				newUser->FD_ID = new_fd;
				//putting the new cUserInfo object in the vector
				vUserInfo.push_back(newUser);
				userCount++;
				bAuthenticProcess = true;

				Authentication::ServerRequest firstRequest;
				std::string requestMessage = "Please input your email address and password and choose 1->Register or 2->Authenticate.";
				int strLength = requestMessage.length();
				firstRequest.set_request(requestMessage);
				firstRequest.set_prefixlength(strLength + 4);

				std::string serializedString;
				firstRequest.SerializeToString(&serializedString);

				int resultInt = send(new_fd, serializedString.c_str(), serializedString.length(), 0);

				if (resultInt == SOCKET_ERROR)
				{
					std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
					closesocket(new_fd);
					WSACleanup();
				}
				continue;
			}//if listeningSocket

	 /************ Recieve incoming messages ******************/
			char recvbuf[DEFAULT_BUFLEN];
			int incomingMessageLength = 0;
			if (bAuthenticProcess)
			{
				//Recieve the message about email and password
				incomingMessageLength = recv(i_fd, recvbuf, sizeof(recvbuf), 0);

				Authentication::UserInput userMessage;
				std::string recevStr(incomingMessageLength, ' ');
				for (int i = 0; i < incomingMessageLength; i++)
				{
					recevStr[i] = recvbuf[i];
				}
				userMessage.ParseFromString(recevStr);
				int length = sizeof(recvbuf);
				int prefixLengthRcvd = userMessage.prefixlength();
				Authentication::UserInput_CommandType commandType = userMessage.command();
				std::string commandTypeStr = userMessage.CommandType_Name(userMessage.command());

				cSendAuthMsg sendMsgToAuthServer;
				sendMsgToAuthServer.pBuffer = &authBuffer;
				sendMsgToAuthServer.email = userMessage.email();
				sendMsgToAuthServer.password = userMessage.password();

				ZeroMemory(recvbuf, 512);

				if (prefixLengthRcvd == 0) continue;

				if (incomingMessageLength != prefixLengthRcvd)
					std::cout << "Error : Packets lengths do not match!" << std::endl;

				/************************************************************************************************/

				if (commandType == Authentication::UserInput_CommandType_REGISTER)
				{
					sendMsgToAuthServer.mType = REGISTER_USER;
				}

				if (commandType == Authentication::UserInput_CommandType_AUTHENTICATE)
				{
					sendMsgToAuthServer.mType = AUTHENTICATE_USER;
				}

				sendMsgToAuthServer.update();
				sendMsgToAuthServer.writeToBuffer();
				resultInt = send(ConnectSocket, sendMsgToAuthServer.pBuffer->toCharArray(), sendMsgToAuthServer.mPacket_length, 0);
				if (resultInt == SOCKET_ERROR)
				{
					std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
					closesocket(ConnectSocket);
					WSACleanup();
					continue;
				}

				cRecvAuthMsg rcvMsgFromAuthServer;
				rcvMsgFromAuthServer.pBuffer = &authBuffer;

				resultInt = recv(ConnectSocket, recvbuf, sizeof(recvbuf), 0);
				if (resultInt > 0)
				{
					rcvMsgFromAuthServer.pBuffer->clear();
					rcvMsgFromAuthServer.pBuffer->loadBuffer(recvbuf, 512);
					rcvMsgFromAuthServer.readFromBuffer();
					if (resultInt != rcvMsgFromAuthServer.mPacket_length)
						std::cout << "Error : Packets lengths do not match!" << std::endl;
					ZeroMemory(recvbuf, 512);
				}

				/******* end of the communtication with Authentication server *************/

				/******* process to reply to the user about Authentication result *************/

				if (commandType == Authentication::UserInput_CommandType_REGISTER)
				{
					Authentication::RegisterResult regResToClient;
					if (rcvMsgFromAuthServer.mType == REGISTRATION_SUCCESSFUL)
					{
						regResToClient.set_registerresult(Authentication::RegisterResult_ResultType_REGISTRATION_SUCCESSFUL);
						bAuthenticProcess = false;
					}
					else if (rcvMsgFromAuthServer.mType == EAMIL_ALREADY_EXISTS)
					{
						regResToClient.set_registerresult(Authentication::RegisterResult_ResultType_EAMIL_ALREADY_EXISTS);
					}
					else
						regResToClient.set_registerresult(Authentication::RegisterResult_ResultType_INTERNAL_SERVER_ERROR);

					regResToClient.set_prefixlength(4 + 4);
					std::string serializedString;
					regResToClient.SerializeToString(&serializedString);

					resultInt = send(i_fd, serializedString.c_str(), serializedString.length(), 0);

					if (resultInt == SOCKET_ERROR)
					{
						std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
						closesocket(i_fd);
						WSACleanup();
						continue;
					}
					if (rcvMsgFromAuthServer.mType != REGISTRATION_SUCCESSFUL)
						continue;//let the user not go to the nex step
				}//if (commandType == Authentication::UserInput_CommandType_REGISTER)

				else if (commandType == Authentication::UserInput_CommandType_AUTHENTICATE)
				{
					Authentication::AuthenticationResult authResToClient;
					if (rcvMsgFromAuthServer.mType == AUTHENTICATE_SUCCESSFUL)
					{
						authResToClient.set_authenticationresult(Authentication::AuthenticationResult_ResultType_AUTHENTICATE_SUCCESSFUL);
						bAuthenticProcess = false;
					}
					else if (rcvMsgFromAuthServer.mType == EAMIL_NOT_EXISTS)
					{
						authResToClient.set_authenticationresult(Authentication::AuthenticationResult_ResultType_EAMIL_NOT_EXISTS);
					}
					else if (rcvMsgFromAuthServer.mType == PASSWORD_NOT_MATCH)
					{
						authResToClient.set_authenticationresult(Authentication::AuthenticationResult_ResultType_PASSWORD_NOT_MATCH);
					}
					else
						authResToClient.set_authenticationresult(Authentication::AuthenticationResult_ResultType_INTERNAL_SERVER_ERROR);

					std::string accountCreatDate = rcvMsgFromAuthServer.dateString;
					authResToClient.set_date(accountCreatDate);
					int stringLength = accountCreatDate.length();
					authResToClient.set_prefixlength(4 + 4 + stringLength);

					std::string serializedString;
					authResToClient.SerializeToString(&serializedString);

					resultInt = send(i_fd, serializedString.c_str(), serializedString.length(), 0);
					if (resultInt == SOCKET_ERROR)
					{
						std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
						closesocket(i_fd);
						WSACleanup();
						continue;
					}
					if (rcvMsgFromAuthServer.mType != AUTHENTICATE_SUCCESSFUL)
					{
						bAuthenticProcess = true;
						continue;
					}//let the user not go to the nex step
				}//if (commandType == Authentication::UserInput_CommandType_AUTHENTICATE)
			
			}//if(bAuthenticProcess)

	  /************ now server starts chat service ******************/
			if (!bAuthenticProcess)
			{
				//Recieve the message
				incomingMessageLength = 0;
				incomingMessageLength = recv(i_fd, recvbuf, sizeof recvbuf, 0);

				if (incomingMessageLength <= 0) continue;

				cReceiveMessage rcvMessage;
				rcvMessage.pBuffer = &buffer;
				rcvMessage.pBuffer->clear();
				rcvMessage.pBuffer->loadBuffer(recvbuf, 512);
				rcvMessage.readFromBuffer();

				if (rcvMessage.mPacket_length == 0) continue;

				if (incomingMessageLength != rcvMessage.mPacket_length)
					std::cout << "Error : Packets lengths do not match!" << std::endl;

				if (rcvMessage.mType == JOIN)
				{
					newUser->userName = rcvMessage.senderName;
					newUser->roomNumber = rcvMessage.mRoomNumber;
				}
				ZeroMemory(recvbuf, 512);

				int index = 0;
				for (int i = 0; i != vUserInfo.size(); i++)
				{
					if (vUserInfo[i]->FD_ID == i_fd)
						index = i;
				}

				cSendMessage serversMessage;
				serversMessage.pBuffer = &buffer;
				serversMessage.mRoomNumber = vUserInfo[index]->roomNumber;
				serversMessage.senderName = vUserInfo[index]->userName;
				serversMessage.mType = rcvMessage.mType;

				if (rcvMessage.mType == JOIN)
				{
					serversMessage.senderName = "Server";
					serversMessage.message = rcvMessage.senderName + " has just joined in Room " + std::to_string(rcvMessage.mRoomNumber) + ". ";
					serversMessage.update();
					serversMessage.writeToBuffer();
					for (auto x : vUserInfo)
					{
						if (x->roomNumber == rcvMessage.mRoomNumber)
						{
							int iSendResult = send(x->FD_ID, serversMessage.pBuffer->toCharArray(), serversMessage.mPacket_length, 0);
							if (iSendResult == SOCKET_ERROR)
							{
								std::cout << "Send failed: " << WSAGetLastError() << std::endl;
								closesocket(x->FD_ID);
								WSACleanup();
							}

						}
					}
				}
				else if (rcvMessage.mType == LEAVE)
				{
					serversMessage.message = rcvMessage.senderName + " has just left Room " + std::to_string(rcvMessage.mRoomNumber) + ". ";
					serversMessage.update();
					serversMessage.writeToBuffer();
					for (auto x : vUserInfo)
					{
						if (x->roomNumber == rcvMessage.mRoomNumber)
						{
							int iSendResult = send(x->FD_ID, serversMessage.pBuffer->toCharArray(), serversMessage.mPacket_length, 0);
							if (iSendResult == SOCKET_ERROR)
							{
								std::cout << "Send failed: " << WSAGetLastError() << std::endl;
								closesocket(x->FD_ID);
								WSACleanup();
							}

						}
					}
					shutdown(vUserInfo[index]->FD_ID, SD_SEND);
					delete vUserInfo.at(index); //free the heap memory
					std::vector<cUserInfo*>::iterator itr = vUserInfo.begin();
					vUserInfo.erase(itr + index);
					userCount--;
				}
				else
				{
					serversMessage.message = rcvMessage.message;
					serversMessage.update();
					serversMessage.writeToBuffer();
					for (auto x : vUserInfo)
					{
						if (x->roomNumber == rcvMessage.mRoomNumber)
						{
							int iSendResult = send(x->FD_ID, serversMessage.pBuffer->toCharArray(), serversMessage.mPacket_length, 0);
							if (iSendResult == SOCKET_ERROR)
							{
								std::cout << "Send failed: " << WSAGetLastError() << std::endl;
								closesocket(x->FD_ID);
								WSACleanup();
							}

						}
					}

				}
			}//if (!bAuthenticProcess)
		} //for-Loop

	} //End of Main loop
#pragma endregion

	// shutdown the send half of the connection since no more data will be sent
	resultInt = shutdown(listeningSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed:" << WSAGetLastError << std::endl;
		closesocket(listeningSocket);
		WSACleanup();
		return 1;
	}

	//Final clean up
	closesocket(listeningSocket);
	WSACleanup(); 

	std::cout << "Disconnect..............." << std::endl;

	for (auto x : vUserInfo)
	{
		delete x;
	}

	return 0;
}