/*
* Program Name : Assignment02 Client.cpp
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : Client program using Google Protocol Buffer and Authentication service
* Date         : Nov. 11, 2016
* Coder        : Jaehuhn Park
*/

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <thread>

#include "Buffer.h"
#include "ProtocolElements.h"

#include "Authentication.pb.h"


//link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

bool bConnected;
std::string clientName;
int chatRoomNumber;

enum eSteps
{
	FIRST_CONNECTION,
	WAIT_FOR_REGISTER,
	WAIT_FOR_AUTHENTICATE,
	ENTER_TO_CHAT_ROOM,
	START_TO_CHAT
} currentStep;

void sendingThread(LPVOID pConnectSocket)
{
	while (bConnected)
	{   	
		Sleep(500);
		if (currentStep == FIRST_CONNECTION)
		{
			Authentication::UserInput userInput;
			int commandType;
			do
			{
				std::cout << "Please choose 1->Register or 2->Authenticate : ";
				std::string tempStr = "";
				std::getline(std::cin, tempStr);
				commandType = (messageType)std::stoi(tempStr);
				if (commandType == 1)
				{
					userInput.set_command(Authentication::UserInput_CommandType_REGISTER);
					currentStep = WAIT_FOR_REGISTER;
				}
				if (commandType == 2)
				{
					userInput.set_command(Authentication::UserInput_CommandType_AUTHENTICATE);
					currentStep = WAIT_FOR_AUTHENTICATE;
				}
			} while (commandType != 1 && commandType != 2);

			int messageLength = 4; //length of itself
			messageLength += sizeof(commandType);

			std::cout << "Your email address : ";
			std::string emailStr = "";
			std::getline(std::cin, emailStr);
			userInput.set_email(emailStr);
			messageLength += emailStr.length();

			std::cout << "Password : ";
			std::string passwordStr = "";
			std::getline(std::cin, passwordStr);
			userInput.set_password(passwordStr);
			messageLength += passwordStr.length();

			userInput.set_prefixlength(messageLength);

			std::string serializedString;
			userInput.SerializeToString(&serializedString);

			int resultInt = send((SOCKET)pConnectSocket, serializedString.c_str(), serializedString.length(), 0);
			if (resultInt == SOCKET_ERROR)
			{
				std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
				closesocket((SOCKET)pConnectSocket);
				WSACleanup();
			}	
			
		} //if (currentStep == FIRST_CONNECTION)

		else if (currentStep == ENTER_TO_CHAT_ROOM)
		{
			Buffer buffer(0);
			cSendMessage aSendingMessage;
			aSendingMessage.mType = JOIN;
			std::cout << "Please choose the chat room among 1 to 10 : ";
			std::string tempStr = "";
			std::getline(std::cin, tempStr);
			chatRoomNumber = (messageType)std::stoi(tempStr);
			aSendingMessage.mRoomNumber = chatRoomNumber;

			std::cout << "Your name : ";
			std::getline(std::cin, clientName);
			aSendingMessage.senderName = clientName;

			aSendingMessage.update(); //updates all the lengths of the packets
			aSendingMessage.pBuffer = &buffer;
			aSendingMessage.writeToBuffer();

			int resultInt = send((SOCKET)pConnectSocket, aSendingMessage.pBuffer->toCharArray(), aSendingMessage.mPacket_length, 0);
			if (resultInt == SOCKET_ERROR)
			{
				std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
				closesocket((SOCKET)pConnectSocket);
				WSACleanup();
			}
			currentStep = START_TO_CHAT;
		} //if (currentStep == ENTER_TO_CHAT_ROOM)

		else if (currentStep == START_TO_CHAT)
		{
			Buffer buffer(0);
			cSendMessage aSendingMessage;
			aSendingMessage.mRoomNumber = chatRoomNumber;
			aSendingMessage.senderName = clientName;

			std::cout << "\nPlease choose: MESSAGE->1, LEAVE->2 : ";
			std::string typeNumber = "";
			std::getline(std::cin, typeNumber);
			aSendingMessage.mType = (messageType)std::stoi(typeNumber); //store the type of the message to send

			if (aSendingMessage.mType != LEAVE)
			{
				std::cout << "Type your message:";
				std::getline(std::cin, aSendingMessage.message);
			}
			aSendingMessage.update(); //updates all the lengths of the packets
			aSendingMessage.pBuffer = &buffer;
			aSendingMessage.writeToBuffer();

			int resultInt = send((SOCKET)pConnectSocket, aSendingMessage.pBuffer->toCharArray(), aSendingMessage.mPacket_length, 0);
			if (resultInt == SOCKET_ERROR)
			{
				std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
				closesocket((SOCKET)pConnectSocket);
				WSACleanup();
			}
			if (aSendingMessage.mType == LEAVE) bConnected = false;
		} //if (currentStep == START_TO_CHAT)
		
	}//end of while Loop
}


int __cdecl main(int argc, char **argv)
{
	currentStep = FIRST_CONNECTION;
	Buffer buffer(0);
#pragma region WinSock_Initialization
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	char recvbuf[DEFAULT_BUFLEN];
	int resultInt;
	int recvbuflen = DEFAULT_BUFLEN;

	//Step 1
	// Initialize Winsock
	resultInt = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	resultInt = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
	if (resultInt != 0) {
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup();
		return 1;
	}
#pragma endregion

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{
		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET)
		{
			std::cout << "Socket failed with error: " << WSAGetLastError() << std::endl;
			WSACleanup();
			return 1;
		}
		/******************* Connect to server ******************************/
		resultInt = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (resultInt == SOCKET_ERROR)
		{
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	} //end of for loop

	std::cout << "Connected to the server" << std::endl;
	bConnected = true;
	freeaddrinfo(result);
	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to server!" << std::endl;
		WSACleanup();
		return 1;
	}

	//Start a thread that takes keyboard input and sends
	std::thread sendThread(sendingThread, (LPVOID)ConnectSocket);

	// Do-While Loop for receiving messages
	do
	{
		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		std::string recevStr(resultInt, ' ');
		for (int i=0; i < resultInt; i++)
		{
			recevStr[i] = recvbuf[i];
		}
		if (resultInt > 0)
		{
			if (currentStep == FIRST_CONNECTION)
			{
				Authentication::ServerRequest msgFromServer;
				bool success = msgFromServer.ParseFromString(recevStr);
				if (!success) {
					std::cout << "Failed to parse message from Server" << std::endl;
				}
				std::cout << "Message from server : " << msgFromServer.request() << std::endl;
				ZeroMemory(recvbuf, 512);
			}
			else if (currentStep == WAIT_FOR_REGISTER)
			{
				Authentication::RegisterResult msgFromServer;
				bool success = msgFromServer.ParseFromString(recevStr);
				if (!success) {
					std::cout << "Failed to parse message from Server" << std::endl;
				}
				if (msgFromServer.registerresult() == Authentication::RegisterResult_ResultType_REGISTRATION_SUCCESSFUL)
				{
					std::cout << "You have been successfully registered and logged in!" << std::endl << std::endl;
					currentStep = ENTER_TO_CHAT_ROOM;
				}
				else if (msgFromServer.registerresult() == Authentication::RegisterResult_ResultType_EAMIL_ALREADY_EXISTS)
				{
					std::cout << "The email address you entered is already registered." << std::endl;
					std::cout << "Please choos option 2->Authentication or input differnt email address." << std::endl;
					currentStep = FIRST_CONNECTION;
				}
				else if (msgFromServer.registerresult() == Authentication::RegisterResult_ResultType_INTERNAL_SERVER_ERROR)
				{
					std::cout << "Sorry, we have some system problem.." << std::endl;
					std::cout << "Please try again later." << std::endl;
					currentStep = FIRST_CONNECTION;
				}
				ZeroMemory(recvbuf, 512);
			}
			else if (currentStep == WAIT_FOR_AUTHENTICATE)
			{
				Authentication::AuthenticationResult msgFromServer;
				bool success = msgFromServer.ParseFromString(recevStr);
				if (!success) {
					std::cout << "Failed to parse message from Server" << std::endl;
				}
				if (msgFromServer.authenticationresult() == Authentication::AuthenticationResult_ResultType_AUTHENTICATE_SUCCESSFUL)
				{
					std::cout << "You have been successfully logged in!" << std::endl;
					std::cout << "Your account was created on " << msgFromServer.date() << std::endl << std::endl;
					currentStep = ENTER_TO_CHAT_ROOM;
				}
				else if (msgFromServer.authenticationresult() == Authentication::AuthenticationResult_ResultType_EAMIL_NOT_EXISTS)
				{
					std::cout << "The email address you entered is not been registered." << std::endl;
					std::cout << "Please choos option 1->Register or try with differnt email address." << std::endl;
					currentStep = FIRST_CONNECTION;
				}
				else if (msgFromServer.authenticationresult() == Authentication::AuthenticationResult_ResultType_PASSWORD_NOT_MATCH)
				{
					std::cout << "The password you entered is not right." << std::endl;
					std::cout << "Please try again." << std::endl;
					currentStep = FIRST_CONNECTION;
				}
				else if (msgFromServer.authenticationresult() == Authentication::AuthenticationResult_ResultType_INTERNAL_SERVER_ERROR)
				{
					std::cout << "Sorry, we have some system problem.." << std::endl;
					std::cout << "Please try again later." << std::endl;
					currentStep = FIRST_CONNECTION;
				}
				ZeroMemory(recvbuf, 512);
			}
			else if (currentStep == ENTER_TO_CHAT_ROOM || currentStep == START_TO_CHAT)
			{
				cReceiveMessage rcvMessage;
				rcvMessage.pBuffer = &buffer;
				rcvMessage.pBuffer->clear();
				rcvMessage.pBuffer->loadBuffer(recvbuf, 512);
				rcvMessage.readFromBuffer();
				if (resultInt != rcvMessage.mPacket_length)
					std::cout << "Error : Packets lengths do not match!" << std::endl;

				if (rcvMessage.mType != UNKNOWN)
				{
					if (rcvMessage.mRoomNumber > 0)
						std::cout << "Room Number: " << rcvMessage.mRoomNumber << std::endl;
					std::cout << "Message From: " << rcvMessage.senderName << std::endl;
					std::cout << "Recieved Message: " << rcvMessage.message << std::endl;
				}
				ZeroMemory(recvbuf, 512);
			}
		}


	} while (resultInt > 0); //end of Do-While Loop

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	std::cout << "Disconnected" << std::endl;

	//Keep the window open
	std::cout << "\nwaiting on exit";
	int tempInput;
	std::cin >> tempInput;

	return 0;
}
