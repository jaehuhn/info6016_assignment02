/*
* Program Name : Assignment01 ProtocolElements.h
* Purpose      : Assignment #1 - INFO-6016 Network Programming
* Description  : Declaration of classes for the Protocol
* Date         : Oct. 21, 2016
* Coder        : Jaehuhn Park
*/

#pragma once
#include "Buffer.h"
#include <string>

enum messageType
{
	JOIN,
	MESSAGE,
	LEAVE,
	UNKNOWN
};

class cSendMessage
{
public:
	cSendMessage();
	void writeToBuffer();
	void update();

	int mPacket_length;
	messageType mType;
	int messageLength;
	std::string message;
	int senderNameLength;
	std::string senderName;
	int mRoomNumber;

	Buffer* pBuffer;
};

class cReceiveMessage
{
public:
	void readFromBuffer();

	int mPacket_length;
	messageType mType;
	int messageLength;
	std::string message;
	int senderNameLength;
	std::string senderName;
	int mRoomNumber;

	Buffer* pBuffer;
};

