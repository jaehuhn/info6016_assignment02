/*
* Program Name : Assignment02 ProtocolElements.cpp
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : Implement the Protocol
* Date         : Oct. 21, 2016
* Coder        : Jaehuhn Park
*/

#include "ProtocolElements.h"

cSendMessage::cSendMessage()
{
	this->mPacket_length = 0;
	this->mType = UNKNOWN;
	this->messageLength = 0;
	this->message = "";
	this->senderNameLength = 0;
	this->senderName = "";
	this->mRoomNumber = 0;
	this->pBuffer = 0;
}

void cSendMessage::writeToBuffer()
{
	this->pBuffer->clear();
	this->pBuffer->writeInt32BE(this->mPacket_length);
	this->pBuffer->writeInt32BE(this->mType);
	this->pBuffer->writeInt32BE(this->messageLength);
	this->pBuffer->writeStringBE(this->message);
	this->pBuffer->writeInt32BE(this->senderNameLength);
	this->pBuffer->writeStringBE(this->senderName);
	this->pBuffer->writeInt32BE(this->mRoomNumber);
}

void cSendMessage::update()
{
	this->messageLength = this->message.length();
	this->senderNameLength = this->senderName.length();
	this->mPacket_length = 4 + 4 + 4 + 4 + 4 +this->messageLength + this->senderNameLength;
}

void cReceiveMessage::readFromBuffer()
{
	this->mPacket_length = this->pBuffer->readInt32BE();
	this->mType = (messageType)this->pBuffer->readInt32BE();
	this->messageLength = this->pBuffer->readInt32BE();
	this->message = this->pBuffer->readStringBE(this->messageLength);
	this->senderNameLength = this->pBuffer->readInt32BE();
	this->senderName = this->pBuffer->readStringBE(this->senderNameLength);
	this->mRoomNumber = this->pBuffer->readInt32BE();
	this->pBuffer->clear();
}

