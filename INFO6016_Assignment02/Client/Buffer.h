/*
* Program Name : Assignment01 Buffer.h for Client
* Purpose      : Assignment #1 - INFO-6016 Network Programming
* Description  :
* Date         : Oct. 21, 2016
* Coder        : Jaehuhn Park
*/

#ifndef __BUFFER_H
#define __BUFFER_H

#include <stdint.h>
#include <string>
#include <vector>

class Buffer {
public:
	Buffer(std::size_t size);
	virtual ~Buffer();

	void writeInt32BE(std::size_t index, int value);
	void writeInt32BE(int value);
	int readInt32BE(std::size_t index);
	int readInt32BE();

	void writeShort16BE(std::size_t index, short int value);
	void writeShort16BE(short int value);
	short int readShort16BE(std::size_t index);
	short int readShort16BE();

	void writeStringBE(std::string value);
	void writeStringBE(std::size_t index, std::string value);
	std::string Buffer::readStringBE(std::size_t stringLength);
	std::string Buffer::readStringBE(std::size_t index, std::size_t stringLength);

	std::size_t getReadIndex();
	std::size_t getWriteIndex();

	void setReadIndex(std::size_t index);
	void setWriteIndex(std::size_t index);

	void clear();
	void growBuffer(size_t growAmount);
	void printInHex();
	void loadBuffer(char recvbuf[512], size_t size);
	const char* toCharArray();

	int getBufferSize();

private:
	std::vector<uint8_t> _buffer;
	int writeIndex;
	int readIndex;
	std::string sendingString; 
	// a string used as a place holder in sending the buffer. We need the variable to stick around since we are pointing to it in a later function
};


#endif
