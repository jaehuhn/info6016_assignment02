/*
* Program Name : Assignment02 AuthenticationServer.cpp
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : For the Authentication service
* Date         : Nov. 11, 2016
* Coder        : Jaehuhn Park
*/

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

#include "sqlite3.h"
#include "SHA256.h"

#include "Buffer.h"
#include "AuthenticProtocol.h"

// for windows socket link (link with Ws2_32.lib)
#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27016"
#define DEFAULT_BUFLEN 512

std::string resultStr;

//This function will be called for every row of data returned in the select statement
static int selectCallback(void *data, int argc, char **argv, char **azColName)
{
	resultStr = "";
	for (int i = 0; i < argc; i++)
	{
		resultStr = argv[i] ;
	}
	return 0;
}

char getRandChar()
{
	int tempInt = 48 + rand() % (122 - 48);
	return static_cast<char>(tempInt);
}


int main()
{
	sqlite3 *database;
	char *errorMessage = 0;
	int returnCode;

	//Opening the database
	returnCode = sqlite3_open("AuthenticationService.db", &database);

	//Test to see if the database opened correctly
	if (returnCode)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(database) << std::endl;
		return(0);
	}

	char* sql = "CREATE TABLE IF NOT EXISTS WEB_AUTH_TABLE("  \
		"ID BIGINT PRIMARY KEY AUTOINCREMENT," \
		"EMAIL       VARCHAR(255)," \
		"HASH        CHAR(64)," \
		"SALT        CHAR(64)," \
		"USERID      BIGINT );";

	returnCode = sqlite3_exec(database, sql, NULL, 0, &errorMessage);
	if (returnCode != SQLITE_OK)
	{
		std::cout << "SQL error: " << errorMessage << std::endl;
		sqlite3_free(errorMessage);
	}

	sql = "CREATE TABLE IF NOT EXISTS USERTABLE("  \
		"ID BIGINT PRIMARY KEY AUTOINCREMENT," \
		"LAST_LOGIN DATETIME DEFAULT CURRENT_TIMESTAMP," \
		"CREATION_DATE DATETIME );";

	returnCode = sqlite3_exec(database, sql, NULL, 0, &errorMessage);
	if (returnCode != SQLITE_OK)
	{
		std::cout << "SQL error: " << errorMessage << std::endl;
		sqlite3_free(errorMessage);
	}


	//instantiate a buffer object with size of 0 & instantiate protocol objects
	Buffer buffer(0);
	cSendAuthMsg sendMessage;
	sendMessage.pBuffer = &buffer;
	cRecvAuthMsg rcvMessage;
	rcvMessage.pBuffer = &buffer;

	std::string dbEmail = "";
	std::string dbPassword = "";
	std::string dateString = "";
	std::string sqlStr;
	bool bInternalError = false;

#pragma region WinSock_Initialization
	//Create a Windows Socket Application Data Object
	WSADATA wsaData;
	//to give Winsock version number and store the DLL information into wsaData
	int resultInt = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}
	struct addrinfo *result = NULL;
	struct addrinfo hints;

	ZeroMemory(&hints, sizeof(hints)); //hints is filled with zeros now

	hints.ai_family = AF_INET; //IPv4 Internet protocols  
	hints.ai_socktype = SOCK_STREAM; //Socket type is TCP
	hints.ai_protocol = IPPROTO_TCP; //Protocol is TCP
	hints.ai_flags = AI_PASSIVE; //for wildcard IP address

	resultInt = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result); //result will contain response information about the host
	if (resultInt != 0)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup(); //to clean up the WinSock instance
		return 1;
	}
#pragma endregion

#pragma region Listening_socket_Create_&_Bind
	SOCKET listeningSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		WSACleanup();
		return 1;
	}
	u_long iMode = 1;
	//To enable non-blocking mode, pass FIONBIO and non zero value of iMode
	ioctlsocket(listeningSocket, FIONBIO, &iMode);

	//Bind the socket
	resultInt = bind(listeningSocket, result->ai_addr, (int)result->ai_addrlen);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket binding failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		closesocket(listeningSocket); //Close the socket
		WSACleanup();
		bInternalError = true;
		return 1;
	}
	freeaddrinfo(result);
#pragma endregion

#pragma region Listening
	if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Auth : Socket listening failed" << std::endl;
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}
	//now waiting for the call
	std::cout << "Auth Server Listening on Socket...." << std::endl << std::endl;

	/*To handle concurrency by Non-blocking using Select()  */
	fd_set master;    //Master list
	fd_set read_fds;  //will copy Master for select() 

	//Clear the master and the temporary sets
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	//Add the listening Socket to the master set
	FD_SET(listeningSocket, &master);

	int new_fd;        //Newly accepted socket descriptor
	int fdmax = listeningSocket; //Maximum file descriptor number 

	//Set timeout waitng  duration
	struct timeval timeValue;
	timeValue.tv_sec = 0;
	timeValue.tv_usec = 500 * 1000; // 500 ms
#pragma endregion

	while (true)
	{
		read_fds = master; // copy the master to the temp fd_set
		//select() checks to see if any socket has activity
		if (select(fdmax + 1, &read_fds, NULL, NULL, &timeValue) == -1)
		{
			std::cout << "Select Error" << std::endl;
			bInternalError = true;
			exit(4);
		}

		//Loop through existing connections looking for data to read
		for (int i_fd = 0; i_fd <= fdmax; i_fd++)
		{
			if (!FD_ISSET(i_fd, &read_fds)) continue;

			if (i_fd == listeningSocket)
			{
				sockaddr_in client_addr;
				socklen_t addrlen = sizeof sockaddr_storage;

				new_fd = accept(listeningSocket, (struct sockaddr *)&client_addr, &addrlen);
				std::cout << "Auth server Connected to: " << client_addr.sin_addr.s_addr << std::endl;
				if (new_fd == -1)
				{
					std::cout << "Accept Error" << std::endl;
					bInternalError = true;
					continue;
				}
				FD_SET(new_fd, &master); // add to master set				
				if (new_fd > fdmax)	fdmax = new_fd; //Keep track of max fd

				std::cout << "New connection on socket : " << new_fd << std::endl;

				continue;
			}//if listeningSocket

	 /************ Recieve incoming messages ******************/
			char recvbuf[DEFAULT_BUFLEN];
			int incomingMessageLength = 0;
			rcvMessage.mType = NOTHING;
			//Recieve the message
			incomingMessageLength = recv(new_fd, recvbuf, sizeof recvbuf, 0);
			//if (incomingMessageLength > 0)
			//{
			rcvMessage.pBuffer->clear();
			rcvMessage.pBuffer->loadBuffer(recvbuf, 512);
			rcvMessage.readFromBuffer();

			if (rcvMessage.mPacket_length == 0) continue;

			if (incomingMessageLength != rcvMessage.mPacket_length)
			{
				std::cout << "Error : Packets lengths do not match!" << std::endl;
				bInternalError = true;
			}

			ZeroMemory(recvbuf, 512);

			dbEmail = rcvMessage.email;
			dbPassword = rcvMessage.password;

			if (rcvMessage.mType == REGISTER_USER)
			{
				resultStr = "";
				sqlStr = "SELECT ID from WEB_AUTH_TABLE WHERE EMAIL = '" + dbEmail + "';";
				returnCode = sqlite3_exec(database, sqlStr.c_str(), selectCallback, NULL, &errorMessage);
				if (returnCode != SQLITE_OK)
				{
					std::cout << "SQL error: " << errorMessage << std::endl;
					sqlite3_free(errorMessage);
					bInternalError = true;
				}

				if (resultStr == "")
				{
					sqlStr = "INSERT INTO USERTABLE (CREATION_DATE) VALUES (CURRENT_TIMESTAMP); ";
					returnCode = sqlite3_exec(database, sqlStr.c_str(), NULL, 0, &errorMessage);
					if (returnCode != SQLITE_OK)
					{
						std::cout << "SQL error: " << errorMessage << std::endl;
						sqlite3_free(errorMessage);
						bInternalError = true;
					}


					std::string newSalt = "";
					for (int i = 0; i < 64; i++)
					{
						newSalt += getRandChar();
					}
					std::string newHash = sha256(newSalt + dbPassword);

					//to get the USERID for WEB_AUTH_TABLE from USERTABLE
					sql = "SELECT ID FROM USERTABLE WHERE ID=(SELECT MAX(ID) FROM USERTABLE);";
					returnCode = sqlite3_exec(database, sql, selectCallback, NULL, &errorMessage);
					if (returnCode != SQLITE_OK)
					{
						std::cout << "SQL error: " << errorMessage << std::endl;
						sqlite3_free(errorMessage);
						bInternalError = true;
					}

					sqlStr = "";
					sqlStr = "INSERT INTO WEB_AUTH_TABLE (EMAIL, HASH, SALT, USERID) VALUES ('";
					sqlStr += dbEmail + "', '" + newHash + "', '" + newSalt + "', " + resultStr + "); ";

					returnCode = sqlite3_exec(database, sqlStr.c_str(), NULL, 0, &errorMessage);
					if (returnCode != SQLITE_OK)
					{
						std::cout << "SQL error: " << errorMessage << std::endl;
						sqlite3_free(errorMessage);
						bInternalError = true;
					}
					sendMessage.mType = REGISTRATION_SUCCESSFUL;
				}
				else // already the email is in the DB
				{
					sendMessage.mType = EAMIL_ALREADY_EXISTS;
				}
			}//if (rcvMessage.mType == REGISTER_USER)

			if (rcvMessage.mType == AUTHENTICATE_USER)
			{
				resultStr = "";
				sqlStr = "SELECT ID from WEB_AUTH_TABLE WHERE EMAIL = '" + dbEmail + "';";
				returnCode = sqlite3_exec(database, sqlStr.c_str(), selectCallback, NULL, &errorMessage);
				if (returnCode != SQLITE_OK)
				{
					std::cout << "SQL error: " << errorMessage << std::endl;
					sqlite3_free(errorMessage);
					bInternalError = true;
				}

				if (resultStr == "")
				{
					sendMessage.mType = EAMIL_NOT_EXISTS;
				}
				else // the user is in the DB
				{
					std::string theId = resultStr;
					sqlStr = "SELECT SALT from WEB_AUTH_TABLE WHERE USERID = '" + theId + "';";
					returnCode = sqlite3_exec(database, sqlStr.c_str(), selectCallback, NULL, &errorMessage);
					if (returnCode != SQLITE_OK)
					{
						std::cout << "SQL error: " << errorMessage << std::endl;
						sqlite3_free(errorMessage);
						bInternalError = true;
					}
					std::string dbSalt = resultStr;
					std::string theHash = sha256(dbSalt + dbPassword);

					sqlStr = "SELECT HASH from WEB_AUTH_TABLE WHERE USERID = '" + theId + "';";
					returnCode = sqlite3_exec(database, sqlStr.c_str(), selectCallback, NULL, &errorMessage);
					if (returnCode != SQLITE_OK)
					{
						std::cout << "SQL error: " << errorMessage << std::endl;
						sqlite3_free(errorMessage);
						bInternalError = true;
					}
					std::string dbHash = resultStr;

					if (theHash == dbHash)
					{
						sendMessage.mType = AUTHENTICATE_SUCCESSFUL;
						sqlStr = "SELECT CREATION_DATE from USERTABLE WHERE ID = '" + theId + "';";
						returnCode = sqlite3_exec(database, sqlStr.c_str(), selectCallback, NULL, &errorMessage);
						if (returnCode != SQLITE_OK)
						{
							std::cout << "SQL error: " << errorMessage << std::endl;
							sqlite3_free(errorMessage);
							bInternalError = true;
						}
						sendMessage.dateString = resultStr;
					}
					else
						sendMessage.mType = PASSWORD_NOT_MATCH;

				} //else - the user email is in the DB
			}//if (rcvMessage.mType == AUTHENTICATE_USER)

			if (rcvMessage.mType == UNKNOWN_REQUEST)
			{
				bInternalError = true;
			}

			if (bInternalError)
				sendMessage.mType = INTERNAL_SERVER_ERROR;

			//after checking all the conditions, send the result message to the server
			if (rcvMessage.mType != NOTHING)
			{
				sendMessage.update();
				sendMessage.writeToBuffer();
				int resultInt = send(new_fd, sendMessage.pBuffer->toCharArray(), sendMessage.mPacket_length, 0);
				if (resultInt == SOCKET_ERROR)
				{
					std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
					closesocket(new_fd);
					WSACleanup();
				}
			}
			//}//if (incomingMessageLength > 0)
		} //end of for loop
	}//End of while loop

	// shutdown the send half of the connection since no more data will be sent
	resultInt = shutdown(listeningSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "Auth server - shutdown failed:" << WSAGetLastError << std::endl;
		closesocket(listeningSocket);
		WSACleanup();
		return 1;
	}

	//Final clean up
	closesocket(listeningSocket);
	WSACleanup();


	 //Close database
	sqlite3_close(database);
	std::cout << "Database closed." << std::endl;

	return 0;
}