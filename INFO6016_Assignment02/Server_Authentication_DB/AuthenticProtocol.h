/*
* Program Name : Assignment02 AuthenticProtocol.h
* Purpose      : Assignment #2 - INFO-6016 Network Programming
* Description  : Protocol for communication between server and Authentication server
* Date         : Nov. 11, 2016
* Coder        : Jaehuhn Park
*/

#pragma once
#include "Buffer.h"
#include <string>

enum eAuthMsgType
{
	NOTHING,
	UNKNOWN_REQUEST,
	REGISTER_USER,
	AUTHENTICATE_USER,
	REGISTRATION_SUCCESSFUL, 
	EAMIL_ALREADY_EXISTS, 
	AUTHENTICATE_SUCCESSFUL, 
	EAMIL_NOT_EXISTS, 
	PASSWORD_NOT_MATCH,
	INTERNAL_SERVER_ERROR
};

class cSendAuthMsg
{
public:
	cSendAuthMsg();
	void writeToBuffer();
	void update();

	int mPacket_length;
	eAuthMsgType mType;
	int emailLength;
	std::string email;
	int passwordLength;
	std::string password;
	int dateLength;
	std::string dateString;

	Buffer* pBuffer;
};

class cRecvAuthMsg
{
public:
	void readFromBuffer();

	int mPacket_length;
	eAuthMsgType mType;
	int emailLength;
	std::string email;
	int passwordLength;
	std::string password;
	int dateLength;
	std::string dateString;

	Buffer* pBuffer;
};

